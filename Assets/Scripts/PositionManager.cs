﻿using System;
using UnityEngine;

public class PositionManager : MonoBehaviour
{
    private GameObject player1;
    private GameObject player2;
    private GameObject upperWall, bottomWall, leftWall, rightWall;

    private const float paddleOffset = 20;
    private float previousScreenWidth;
    private float previousScreenHeight;

    private void Start()
    {
        player1 = GameObject.Find("Player01");
        player2 = GameObject.Find("Player02");
        upperWall = GameObject.Find("Upper Wall");
        bottomWall = GameObject.Find("Bottom Wall");
        leftWall = GameObject.Find("Left Wall");
        rightWall = GameObject.Find("Right Wall");

        SetCorrectWallsPosition();
        SetCorrectPlayersPosition();
    }


    private void Update()
    {
        //COMPARING BOTH NEW SCREEN WIDTH AND HEIGHT WITH OLD WIDTH AND HEIGHT
        if (Screen.width != previousScreenWidth || Screen.height != previousScreenHeight)
        {
            SetCorrectWallsPosition();
            SetCorrectPlayersPosition();
        }
    }

    private void SetCorrectPlayersPosition()
    {
        previousScreenWidth = Screen.width;
        previousScreenHeight = Screen.height;

        //PLAYER 1 NEW POSITION
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(player1.transform.position);
        screenPosition.x = paddleOffset;
        player1.transform.position = Camera.main.ScreenToWorldPoint(screenPosition);

        //PLAYER 1 NEW POSITION
        screenPosition = Camera.main.WorldToScreenPoint(player2.transform.position);
        screenPosition.x = previousScreenWidth - paddleOffset;
        player2.transform.position = Camera.main.ScreenToWorldPoint(screenPosition);
    }

    private void SetCorrectWallsPosition()
    {
        float thickness = 3.0f;
        float wider = 3.0f;
        float height = Camera.main.orthographicSize * 2.0f;
        float width = height * Camera.main.aspect;

        //MOVING
        upperWall.transform.position = new Vector2(0, height / 2 + thickness / 2);
        bottomWall.transform.position = new Vector2(0, -(height / 2 + thickness / 2));
        leftWall.transform.position = new Vector2(-(width / 2 + thickness / 2), 0);
        rightWall.transform.position = new Vector2(width / 2 + thickness / 2, 0);

        //RESIZING
        upperWall.GetComponent<BoxCollider2D>().size = new Vector2(width + wider, thickness);
        bottomWall.GetComponent<BoxCollider2D>().size = new Vector2(width + wider, thickness);
        leftWall.GetComponent<BoxCollider2D>().size = new Vector2(thickness, height);
        rightWall.GetComponent<BoxCollider2D>().size = new Vector2(thickness, height);
    }
}
