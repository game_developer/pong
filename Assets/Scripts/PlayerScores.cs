﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerScores : MonoBehaviour 
{
    public static PlayerScores instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        
    }

    public int player1Score = 0, player2Score = 0;
    public string separator = " : ";

    private void Start()
    {
        Ball.GameEnded += GameEnded;
    }

    private void GameEnded(string winnerName)
    {
        if (winnerName == "Player01")
        {
            player1Score++;
        }
        else
        {
            player2Score++;
        }
    }
}
