﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    private const float paddleLength = 1f;

    [SerializeField]
    private float moveSpeed;
    public bool isArrows;
    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Move();

    }

    private void Move()
    {
        rb.velocity = Vector2.zero;
        float axis;

        if (isArrows)
        {
            axis = Input.GetAxisRaw("VerticalArrows");
        }

        else
        {
            axis = Input.GetAxisRaw("VerticalKeys");
        }

        var velocity = axis * moveSpeed;
        rb.velocity = Vector2.up * velocity;
    }
}