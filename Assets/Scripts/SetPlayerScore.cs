﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetPlayerScore : MonoBehaviour 
{
    private Text text;

    private void Start()
    {
        text = GetComponent<Text>();

        string scoreText = PlayerScores.instance.player1Score.ToString() +
            PlayerScores.instance.separator.ToString() + PlayerScores.instance.player2Score.ToString();

        text.text = scoreText;
    }
}
