﻿using UnityEngine;

public class PlayManager : MonoBehaviour
{
    private GameObject canvas;
    
    //THIS WILL NOT BE DESTROYED
    public GameObject ball;
    private GameObject countdown;

    private void Start()
    {
        countdown = GameObject.Find("Countdown");

        //ADDING A METHOD TO THE COUNTDOWN ENDED EVENT
        Countdown.CountdownEnded += CountdownEnded;

        StartCoroutine(countdown.GetComponent<Countdown>().SetCountdownImages());
    }
    private void OnDestroy()
    {
        Countdown.CountdownEnded -= CountdownEnded;
    }
    private void CountdownEnded()
    {
        //DISABLING COUNTDOWN CANVAS
        countdown.SetActive(false);
        Instantiate(ball);
    }
}
