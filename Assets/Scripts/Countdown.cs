﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Countdown : MonoBehaviour
{
    private Image image;

    public float waitTime;
    public Sprite three;
    public Sprite two;
    public Sprite one;
    public Sprite go;

    public static event Action CountdownEnded;

    protected virtual void OnCountdownEnded()
    {
        if (CountdownEnded != null)
        {
            CountdownEnded();
        }
    }

    public IEnumerator SetCountdownImages()
    {
        image = GetComponent<Image>();
        image.sprite = three;
        yield return new WaitForSeconds(waitTime);

        image.sprite = two;
        yield return new WaitForSeconds(waitTime);

        image.sprite = one;
        yield return new WaitForSeconds(waitTime);

        image.sprite = go;
        yield return new WaitForSeconds(waitTime);

        OnCountdownEnded();
    }
}
