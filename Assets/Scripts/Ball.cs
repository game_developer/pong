﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour
{
    #region Variables
    private Rigidbody2D rb;

    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private float speed = 10;

    //TRY TO CONFIGURE IT LATER
    private float friction = 0.5f;

    private float[] firstTouchYVectors = { 1, -1, 2, -2, 3, -3 };

    private bool firstTouch = true;
    #endregion

    public static event Action<string> GameEnded;
    protected virtual void OnGameEnded(string winnerName)
    {
        if (GameEnded != null)
        {
            GameEnded(winnerName);
        }
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        spriteRenderer = GetComponent<SpriteRenderer>();

        rb.velocity = UnityEngine.Random.Range(1, 3) == 1 ? Vector2.right * speed : Vector2.left * speed;
    }

    private void FixedUpdate()
    {
        HandleSpeed();
    }

    private void HandleSpeed()
    {
        if (rb.velocity.magnitude != speed)
        {
            rb.velocity = rb.velocity.normalized * speed;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        Vector2 ballMovementVector = rb.velocity;
        string colliderTag = collision.tag;

        if (colliderTag == "Wall")
        {
            ballMovementVector.y *= -1;
        }

        else if (colliderTag == "Player01" || colliderTag == "Player02")
        {
            ballMovementVector.x *= -1;

            if (firstTouch)
            {
                ballMovementVector.y = firstTouchYVectors[UnityEngine.Random.Range(0, firstTouchYVectors.Length - 1)];
            }
            else
                ballMovementVector.y -= collision.GetComponent<Rigidbody2D>().velocity.y;


            var color = collision.GetComponent<SpriteRenderer>().color;

            spriteRenderer.color = color;
        }

        else if (colliderTag == "Side Wall")
        {
            if (collision.name == "Right Wall")
            {
                OnGameEnded("Player01");
            }
            else
            {
                OnGameEnded("Player02");
            }
            SceneManager.LoadScene("Main");
        }

        rb.velocity = ballMovementVector;
    }
}
